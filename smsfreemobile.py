#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Send SMS on your own mobile by way of Free mobile operator
    You must activate this feature in your Free mobile account:
    Gérer mon compte > Mes options > Notifications par SMS

"""

import sys
import argparse

from urllib.request import urlopen
from urllib.parse import urlencode
from urllib.error import HTTPError, URLError


def sendsms(user, key, msg):
    """

    Arguments:
        user: your user name
        key: private key
        msg: message to send

    Returns :
        200: All gone right. Message sent
        400: Missing parameter
        402: Too much messages in short time
        403: Service not activated in account or bad login / key
        404: File not found
        500: Server error try later

    Example:
        sendsms(user="12345678", key="OcRojxSrhGKPzx", msg="Hello from pyton")

    """

    if not user or not key or not msg:
        return 400

    urlfreesms = "https://smsapi.free-mobile.fr/sendmsg?%s"

    # Request
    param = {"user": user, "pass": key, "msg": msg}

    try:
        response = urlopen(urlfreesms % urlencode(param))

        # Récupération du code de retour HTTP (200, ... ? )
        code = response.getcode()

    except HTTPError as err:
        # Récupération du code d'erreur HTTP (403, ... ?)
        code = err.code

    except URLError as err:
        return "Erreur dans l'url '{}'. Cette dernière pourrait avoir changé. {}".format(
            urlfreesms, err
        )

    except:
        return "Erreur non prévue: {}".format(sys.exc_info()[0])

    return code


if __name__ == "__main__":
    # Command line parsing
    parse_args = {
        "user": (
            "u",
            dict(
                action="store",
                dest="user",
                help="Identifiant Free Mobile",
                required=True,
            ),
        ),
        "key": (
            "k",
            dict(
                action="store", dest="key", help="Clé d'identification", required=True
            ),
        ),
        "msg": (
            "m",
            dict(action="store", dest="msg", help="Message à envoyer", required=True),
        ),
    }

    parser = argparse.ArgumentParser(
        description="Envoi de SMS avec Free Mobile sur votre propre portable"
    )

    for k, v in parse_args.items():
        parser.add_argument("-{}".format(v[0]), "--{}".format(k), **v[1])

    results = parser.parse_args()
    code = sendsms(user=results.user, key=results.key, msg=results.msg)

    messages_dic = {
        200: "Le SMS a bien été envoyé",
        400: "Un des paramètres obligatoire est manquant",
        402: "Trop de SMS ont été envoyés en trop peu de temps",
        403: "Le service n'est pas activé sur l'espace abonné, ou login / clé incorrect",
        404: "Adresse non trouvée",
        500: "Erreur côté serveur. Veuillez essayer ultérieurement",
    }

    print(messages_dic.get(code, "Code de retour HTTP inconnu:\n{}".format(code)))
